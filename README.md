# Step-by-step

Para esta demo se va  aestar trbajancon un un cluster Single Node de OpenShift 4.13.30.

Se tiene, ademas, creado un proyecto de nombre `demo-chained-builds`.

# Despliegue simple con S2I

Esta demostración de despliegue es para mostrar la comparativa entre diferencias de despligue entre una construcción tracicional con el mecanismo de S2I y una con chained builds.

Para hacer un despliegue simple, desde la perspectiva de `Developer` en la concola web de OpenShift, vamos a **+Add**.

Y vamos a importar desde un reporitorio remoto de Git. Click en **Import from Git**.

![Screenshot](assets/readme_images/add-simple.png)

Y ahora en el campo **Git Repo URL** vamos a pegar el URL de este repositorio de Git.

![Screenshot](assets/readme_images/import-from-git.png)

Ahora vamos a bajar y a llenar los campos con la siguiente infomración:

**Application name:**

`react-app-simple-build`

**Name:**

`react-app-simple-build`

**Resource type:**

`Deployment`

**Target port:**

`3000`


`[CHECK]` **Create a route**

Dejamos los demás valores por default. Y damos click en **Create**.

![Screenshot](assets/readme_images/create-simple.png)

Va a comenzar a construirse. Vamos a esperar a qeu el icono se coloqeu de color azul oscuro, que nos indicará que el despliegue se hizo de corma correcta.

![Screenshot](assets/readme_images/simple-building.png)

Una vez finalizado damos click sobre el icono y consultamos la ruta de la aplciación. O tambien podemos abrir la aplicación dando click en la flecha en la esquina superior derecha.

![Screenshot](assets/readme_images/simple-route.png)

Y vamos a ver la presentación de React.

![Screenshot](assets/readme_images/simple-front.png)


# Despliegue utilizando chained builds

Para hacer el despliegue de la aplicación que utiliza chained builds lo vamos a hacer a travez de un template. 
Por lo que para hacer esto, primero vamos a crear el template y luego vamos a instanciarlo y vamos a ver la diferencia del tamaño de las imagenes.

Vamos a rear el template abriendo el editor dentro de la consola de OpenShift, en el icono de más (**+**) en la parte superior derecha de la consola de OpenShift.

![Screenshot](assets/readme_images/mas-icon.png)

Y en este editor vamos a pegar el contenido del archivo YAML `template.yaml` que se encuentra en la raiz de este repositorio.

![Screenshot](assets/readme_images/copy-yaml.png)

Y damos Click en **Create**.

![Screenshot](assets/readme_images/create-template.png)

Ahora, para hacer la construcción y el despliegue de nuestra aplicación, vamos a instanciar este template.
Vamos a la sección **+Add** y bajo **Developer Catalog** damos Click en **All services.**

![Screenshot](assets/readme_images/all-services.png)

Y filtramos por `react-web-app`.

Damos click sobre el template y luego en **Instantiate Template**.

![Screenshot](assets/readme_images/select-template.png)

![Screenshot](assets/readme_images/insta-template.png)

En este caso, solo vamos a modificar los campor **Source URL** y **Source Branch**, con los siguientes datos.

**Source URL**:
`https://gitlab.com/rh-savazque-demo/openshift/chained-builds-react-app.git`

**Source Branch**:
`main`

Y el resto de valores los dejamos por default.

![Screenshot](assets/readme_images/fill-template.png)

Y se da Click en **Create**.

Vamos a ver un DeploymentConfig pendiente, cuya imagen se esta construyendo.

![Screenshot](assets/readme_images/build-1.png)

Si vamos a la sección de **Builds** vamos a ver 2 BuildConfig nuevos, que son los que estan definidos en el Template que acabamos de crear.

![Screenshot](assets/readme_images/builds.png)


Si abrimos el BuildConfig `react-web-app-builder`, y nos vamos a la sección de **Builds** vamos a encontrar que se esta construyendo la imagen qeu genera los artefactos compilados.


![Screenshot](assets/readme_images/build-2.0.png)

Y despues de un par de minutos, deberia termianr de construir, y disparar la cosntruccipon del segundo BuildConfig.

![Screenshot](assets/readme_images/build-2.png)

Si ahora vamos al BuildConfig `static-web-app-running-on-nginx`, igualmente en su seccion **Builds** vamos a ver que acaba de comenzar a construir una imagen, que ya a copiar loos artefactos compoilados de la primera imagen construida.

![Screenshot](assets/readme_images/build-4.png)

Que no deberia tardas mas de unos segundos en generarse, ya que solo necesita copiar los recursos.

![Screenshot](assets/readme_images/build-5.png)

Si ahora regresamos a la topologia, veremos que el DeploymentConfig que estaba pendiente ya se encuentra corriendo.

![Screenshot](assets/readme_images/topology-build.png)

Ahora si consultamos al ruta, veremos que la aplicacion se encuentra corriendo correctamente.

![Screenshot](assets/readme_images/build-running.png)

Ahora vamos a comparar el tamaño de las imagenes.

Vamos a dar Click sobre el icono del primer despliegue. Y seleccionamos el BuildConfig.

![Screenshot](assets/readme_images/open-build-config.png)

En la pestaña **Builds**, damos Click sobre el Build.

![Screenshot](assets/readme_images/select-build.png)

Aqui, en la pestaña de **Details**, en la seccion **Output to**, damos Click sobre el nombre del ImageStreamTag.

![Screenshot](assets/readme_images/image-stream-tag.png)

Y en este, en **Details** vamos a encontrar el tamaño de la imagen.

Que en este caso, su peso es de **283.1 MiB**.

![Screenshot](assets/readme_images/size-1.png)

Si repetimos este mismo proceso, pero para el DeploymentConfig del despliegue que utiliza cvhained build, vamos a ver que esta tiene un tamaño de **108.9 MiB**.

Que es casi una tercera parte del tamaño de la imagen con un despliegue sencillo.

![Screenshot](assets/readme_images/size-2.png)



<br>
<br>
<br>

---

<br>
<br>
<br>


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
